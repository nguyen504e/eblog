import fs from 'fs'
import path from 'path'

import { ObjectID } from 'mongodb'
import { times } from 'lodash'

const data = times(4, (i) => {
  const filepath = path.resolve(__dirname, `./statics/v${i + 1}.jpeg`)
  return { _id: new ObjectID(), data: fs.readFileSync(filepath), type: 'image/jpeg' }
})

export default data
