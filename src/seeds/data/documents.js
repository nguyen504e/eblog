import { ObjectID } from 'mongodb'
import { shuffle, times, without } from 'lodash'

import { random, date, helpers, lorem } from '../faker'
import { someUser } from './users'
import files from './files'

let dataIds = files.map((i) => ({ type: 'picture', data: i._id.toString() }))
times(100 - files.length, () => {
  dataIds.push({ type: 'chart', data: JSON.stringify(helpers.userCard()) })
})

dataIds = shuffle(dataIds)
const documents = times(100, (i) => {
  const created = date.past()
  const updated = date.between(created, new Date())
  const like = someUser()
  const dislike = without(someUser(), ...like)
  const { data, type } = dataIds[i]
  const users = [undefined, undefined, undefined, someUser()]

  return {
    _id: new ObjectID(),
    title: random.words,
    auth: random()._id,
    created,
    updated,
    type,
    content: lorem.paragraph(),
    data,
    react: { like, dislike, countLike: like.length, countDislike: dislike.length },
    users: random.arrayElement(users)
  }
})

export default documents
