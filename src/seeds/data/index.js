import documents from './documents'
import files from './files'
import permissions from './permissions'
import roles from './roles'
import users from './users'

export default {
  permissions,
  roles,
  users,
  documents,
  files
}
