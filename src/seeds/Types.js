export const documentTypes = ['chart', 'post', 'picture']
export const isPictureType = (type) => type === 'picture'
