import { ObjectID } from 'mongodb'

import FileModel from '../models/FileModel'

export default async function(ctx) {
  const file = await FileModel.find(new ObjectID(ctx.params.id))
}
