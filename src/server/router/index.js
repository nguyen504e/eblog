import Router from 'koa-router'

import file from './file'

const router = new Router()

router.get('/file/:id', file)

export default router
