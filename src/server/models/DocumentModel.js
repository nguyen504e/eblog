import { BaseModel } from './BaseModel'
import { Collection, Use } from '../decorators'
import timestamp from './plugins/timestamp'

@Collection()
@Use(timestamp)
class DocumentModel extends BaseModel {}

export default DocumentModel
