import { BaseModel } from './BaseModel'
import { Collection } from '../decorators'

@Collection()
class FileModel extends BaseModel {
}

export default FileModel
