import { GraphQLList, GraphQLObjectType } from 'graphql'

import { $STRING, INT, STRING } from './base/GraphQLTypes'
import { Id, TimeStamp } from './base/Fragments'
import { TypeSetFactory } from './base/GraphQLScalarTypes'
import User, { ListUser } from './User'

const DocumentReact = new GraphQLObjectType({
  name: 'DocumentReact',
  fields: {
    like: { type: ListUser },
    dislike: { type: ListUser },
    countLike: { type: INT },
    countDislike: { type: INT }
  }
})

const Document = new GraphQLObjectType({
  name: 'Document',
  fields: {
    auth: { type: User },
    type: { type: STRING },
    content: { type: STRING },
    data: { type: $STRING },
    react: { type: DocumentReact },
    users: { type: ListUser },
    ...TimeStamp,
    ...Id
  }
})

export const DocumentSet = TypeSetFactory('DocumentSet', Document)
export const ListDocument = new GraphQLList(Document)
export default Document
