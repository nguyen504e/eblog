import { GraphQLList, GraphQLObjectType } from 'graphql'

import { $STRING } from './base/GraphQLTypes'

export const ErrorExtensions = new GraphQLObjectType({ name: 'ErrorExtensions', fields: { code: { type: $STRING } } })
const PropheticError = new GraphQLObjectType({
  name: 'PropheticError',
  fields: { name: { type: $STRING }, message: { type: $STRING }, extensions: { type: ErrorExtensions } }
})
export const ListPropheticError = new GraphQLList(PropheticError)
export default PropheticError
