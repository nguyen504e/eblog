import { GraphQLObjectType } from 'graphql'

import { $LIST_STRING, $STRING } from './base/GraphQLTypes'
import { Id } from './base/Fragments'

export default new GraphQLObjectType({
  name: 'Auth',
  fields: {
    email: { type: $STRING },
    token: { type: $STRING },
    permissions: { type: $LIST_STRING },
    ...Id
  }
})
