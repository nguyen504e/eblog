import { Limit } from '../base/Fragments'
import { ListPermission } from '../Permission'
import { listToJSON } from '../../utils'
import PermissionModel from '../../models/PermissionModel'

export default {
  type: ListPermission,
  args: { ...Limit },
  async resolve(root, { limit }, ctx) {
    // if (!(await checkPermission(ctx, 'USER_MANAGEMENT_VIEW'))) {
    // throw new ForbiddenError()
    // }
    const permissions = await PermissionModel.limit(limit).find()
    return listToJSON(permissions)
  }
}
