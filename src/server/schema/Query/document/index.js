import { DocumentSet } from '../../Document'
import { Limit } from '../../base/Fragments'
import { listToJSON } from '../../../utils'
import DocumentModel from '../../../models/DocumentModel'

export default {
  type: DocumentSet,
  args: { ...Limit },
  async resolve(root, { limit }) {
    console.log('xxxxxx')
    console.log(root)
    try {
      const docs = await DocumentModel.limit(limit)
        .sort({ created: -1 })
        .find()

      return listToJSON(docs)
    } catch (e) {
      throw e
    }
  }
}
