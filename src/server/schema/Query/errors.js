import { forOwn } from 'lodash'

import { ListPropheticError } from '../Error'
import * as errorClasses from '../../Errors'

export default {
  type: ListPropheticError,
  resolve() {
    const list = []
    forOwn(errorClasses, (Cls, name) => {
      const inst = new Cls()
      list.push({
        name,
        message: inst.message,
        extensions: {
          code: inst.extensions.code
        }
      })
    })

    return list
  }
}
