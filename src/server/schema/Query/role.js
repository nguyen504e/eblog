import { ForbiddenError } from 'apollo-server'

import { Limit } from '../base/Fragments'
import { ListRole } from '../Role'
import { listToJSON } from '../../utils'
import RoleModel from '../../models/RoleModel'
import checkPermission from '../../service/checkPermission'

export default {
  type: ListRole,
  args: { ...Limit },
  async resolve({ limit }, ctx) {
    if (!checkPermission(ctx.state.id, 'USER_MANAGEMENT_VIEW')) {
      throw new ForbiddenError()
    }
    const permissions = await RoleModel.limit(limit).find()
    return listToJSON(permissions)
  }
}
