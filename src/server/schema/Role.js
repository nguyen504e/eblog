import { GraphQLList, GraphQLObjectType } from 'graphql'

import { $STRING, LIST_STRING } from './base/GraphQLTypes'
import { Id } from './base/Fragments'

const Role = new GraphQLObjectType({
  name: 'Role',
  fields: { name: { type: $STRING }, permissions: { type: LIST_STRING }, ...Id }
})

export const ListRole = new GraphQLList(Role)
export default Role
