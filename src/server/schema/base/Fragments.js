import { DATE, ID, INT } from './GraphQLTypes'

export const TimeStamp = { created: { type: DATE }, updated: { type: DATE } }
export const Id = { _id: { type: ID } }
export const Limit = { limit: { type: INT, defaultValue: 10 } }
