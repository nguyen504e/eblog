import { GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLString } from 'graphql'

import { GraphQLDateTime, GraphQLObjectId } from './GraphQLScalarTypes'

export const INT = GraphQLInt
export const STRING = GraphQLString
export const LIST_INT = new GraphQLList(GraphQLInt)
export const LIST_STRING = new GraphQLList(GraphQLString)
export const $INT = new GraphQLNonNull(GraphQLInt)
export const $STRING = new GraphQLNonNull(GraphQLString)
export const $LIST_INT = new GraphQLNonNull(new GraphQLList(GraphQLInt))
export const $LIST_STRING = new GraphQLNonNull(new GraphQLList(GraphQLString))
export const LIST_ID = new GraphQLList(GraphQLObjectId)
export const DATE = GraphQLDateTime
export const ID = GraphQLObjectId
