import { GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLScalarType, Kind } from 'graphql'
import moment from 'moment'

export function TypeSetFactory(name, MType) {
  return new GraphQLObjectType({
    name,
    fields: {
      data: { type: new GraphQLList(MType) },
      total: { type: GraphQLInt },
      size: { type: GraphQLInt }
    }
  })
}
export const GraphQLDateTime = new GraphQLScalarType({
  name: 'DateTime',
  description: 'The `DateTime` scalar represents a date and time following the ISO 8601 standard',
  serialize(value) {
    // value sent to the client
    return value
  },
  parseValue(value) {
    // value from the client
    if (!moment(value).isValid) {
      throw new TypeError(`DateTime must be in a recognized RFC2822 or ISO 8601 format ${String(value)}.`)
    }

    return moment.utc(value).toISOString()
  },
  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new TypeError(`DateTime cannot represent non string type ${String(ast.value != null ? ast.value : null)}`)
    }

    return moment.utc(ast.value).toISOString()
  }
})

const objectidPattern = /^[0-9a-fA-F]{24}$/
const isObjectId = (str) => objectidPattern.test(str)

const parseObjectId = (_id) => {
  if (isObjectId(_id)) {
    return ObjectId(_id)
  }

  throw new Error('ObjectId must be a single String of 24 hex characters')
}
export const GraphQLObjectId = new GraphQLScalarType({
  name: 'ObjectId',
  description: 'The `ObjectId` scalar type represents a mongodb unique ID',
  serialize: String,
  parseValue: parseObjectId,
  parseLiteral: (ast) => parseObjectId(ast.value)
})
