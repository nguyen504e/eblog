import { GraphQLInputObjectType } from 'graphql'

import { $STRING, INT } from './GraphQLTypes'

export const PageInput = new GraphQLInputObjectType({
  name: 'PageInput',
  fields: () => ({
    size: { type: INT, defaultValue: 10 },
    number: { type: INT, defaultValue: 1 },
    total: { type: INT }
  })
})

export const ContinuousInput = new GraphQLInputObjectType({
  name: 'ContinuousInput',
  fields: () => ({
    index: { type: $STRING },
    size: { type: INT, defaultValue: 10 }
  })
})
