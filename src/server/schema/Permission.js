import { GraphQLList, GraphQLObjectType } from 'graphql'

import { $STRING } from './base/GraphQLTypes'
import { Id } from './base/Fragments'

const Permission = new GraphQLObjectType({ name: 'Permission', fields: { name: { type: $STRING }, ...Id } })
export const ListPermission = new GraphQLList(Permission)
export default Permission
