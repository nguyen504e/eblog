import { GraphQLInputObjectType, GraphQLList, GraphQLObjectType } from 'graphql'

import { $STRING, LIST_ID, STRING } from './base/GraphQLTypes'
import { Id, TimeStamp } from './base/Fragments'
import { ListRole } from './Role'
import { TypeSetFactory } from './base/GraphQLScalarTypes'

const User = new GraphQLObjectType({
  name: 'User',
  fields: {
    firstName: { type: STRING },
    lastName: { type: STRING },
    email: { type: $STRING },
    role: { type: ListRole },
    ...TimeStamp,
    ...Id
  }
})

export const ListUser = new GraphQLList(User)
export const UserSet = TypeSetFactory('UserSet', User)
export const UserFilterInput = new GraphQLInputObjectType({
  name: 'UserFilterInput',
  fields: () => ({
    firstName: { type: STRING },
    lastName: { type: STRING },
    email: { type: STRING },
    role: { type: LIST_ID },
    ...TimeStamp
  })
})

export default User
